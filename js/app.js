

const headerIndex = document.querySelector('.header-index');

if (headerIndex) {
    let scrlAll = window.scrollY
    window.addEventListener('scroll', () => {
        let scrl = window.scrollY
        if (scrl > 30) {
            headerIndex.classList.add('active')
        }
        if (scrl < 30) {
            headerIndex.classList.remove('active')
        }
    })
    if (scrlAll > 30) {
        headerIndex.classList.add('active')
    }
    if (scrlAll < 30) {
        headerIndex.classList.remove('active')
    }
}

const footerItemList = document.querySelectorAll('.footer .item .list');

if (footerItemList) {
    footerItemList.forEach(item => {
        item.querySelector('span').addEventListener('click', () => {
            item.classList.toggle('active');
        })
    })
}


const modalMenuChange = document.querySelector('.modal-menu-change');

if (modalMenuChange) {
    modalMenuChange.querySelectorAll('.mlistActive').forEach((item, index) => {
        item.addEventListener('click', () => {
            item.parentNode.classList.toggle('active')
        })
    })
}

const menuM = document.querySelector('.menu-m'),
modalMenu = document.querySelector('.modal-menu'),
modal = document.querySelectorAll('.modal'),
modalClose = document.querySelectorAll('.modal-close'),
modalBg = document.querySelectorAll('.modal-bg'),
openHistoryPrice = document.querySelectorAll('.open-history-price'),
modalHistoryPrice = document.querySelector('.modal-history-price');

if (openHistoryPrice) {
    openHistoryPrice.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault()
            document.body.style.overflow = 'hidden'
            modalHistoryPrice.classList.add('active')
        })  
    })
}

if (menuM) {
    menuM.addEventListener('click', () => {
        modalMenu.classList.add('active')
        document.body.style.overflow = 'hidden'
    })
}
if (modalBg || modalClose) {
    modalBg.forEach(item => {
        item.addEventListener('click', () => {
            modal.forEach(elem => {
                elem.classList.remove('active')
                document.body.style.overflow = ''
            })
        })
    })
    modalClose.forEach(item => {
        item.addEventListener('click', () => {
            modal.forEach(elem => {
                elem.classList.remove('active')
                document.body.style.overflow = ''
            })
        })
    })
}

const catalogFilterTitle = document.querySelector('.catalog-params-filter-vis');

if (catalogFilterTitle) {
    catalogFilterTitle.addEventListener('click', () => {
        catalogFilterTitle.classList.toggle('active')
        document.querySelector('.catalog-row-menu').classList.add('active')
        document.querySelector('.products-wrapper').classList.add('active')
        document.querySelector('.catalog-params').classList.add('active')
    })
    document.querySelector('.catalog-filter-close').addEventListener('click', () => {
        catalogFilterTitle.classList.remove('active')
        document.querySelector('.catalog-row-menu').classList.remove('active')
        document.querySelector('.products-wrapper').classList.remove('active')
        document.querySelector('.catalog-params').classList.remove('active')
    })
    document.querySelector('.products-wrapper-bg').addEventListener('click', () => {
        catalogFilterTitle.classList.remove('active')
        document.querySelector('.catalog-row-menu').classList.remove('active')
        document.querySelector('.products-wrapper').classList.remove('active')
        document.querySelector('.catalog-params').classList.remove('active')
    })
}

const mapFilter = document.querySelector('.map-filter'),
mapSection = document.querySelector('.map-section');

if (mapFilter) {
    mapFilter.addEventListener('click', () => {
        mapSection.classList.add('active')
    })
    mapSection.querySelector('.catalog-filter-close').addEventListener('click', () => {
        mapSection.classList.remove('active')
    })
    mapSection.querySelector('.map-bg').addEventListener('click', () => {
        mapSection.classList.remove('active')
    })
}

const catalogPagePreview = document.querySelectorAll('.catalog-page-preview .tabs .item');

let countTabs = 0

if (catalogPagePreview[0]) {

    const tabsContentPhoto = document.querySelectorAll('.catalog-page-preview .tabs-content .tabs-content-item');

    catalogPagePreview.forEach((item, index) => {
        item.addEventListener('click', () => {
            countTabs = index
            changeTabsItem()
        })
    })

    const changeTabsItem = () => {
        catalogPagePreview.forEach(item => {
            item.classList.remove('active')
        })
        tabsContentPhoto.forEach(item => {
            item.classList.remove('active')
        })
        catalogPagePreview[countTabs].classList.add('active');
        tabsContentPhoto[countTabs].classList.add('active');
    }
    changeTabsItem()
}

const langDrop = document.querySelectorAll('.lang-drop');

if (langDrop) {
    langDrop.forEach(item => {
        item.addEventListener('click', (e) => {
            e.preventDefault();
            if (e.target.closest('.lang-title')) {
                item.classList.toggle('active')
            }
        })
    })

}

const swiper = new Swiper('.swiper-img', {
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
        el: '.swiper-pagination-img',
        dynamicBullets: true,
    },
    navigation: {
        nextEl: '.swiper-button-next-img',
        prevEl: '.swiper-button-prev-img',
    }
})
const swiperPage = new Swiper('.swiper-page', {
    autoHeight: true,
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
        el: '.swiper-pagination-page',
        clickable: true
    },
    on: {
        init: function(params) {
            let count = 0
            document.querySelectorAll('.startCount').forEach(item => {
                count += item.querySelectorAll('.item').length
            })
            document.querySelector('.swiper-page-count').textContent = '+' + count
        }
    }
})

const swiperAbout = new Swiper('.swiperAbout', {
    // Default parameters
    slidesPerView: 1.2,
    spaceBetween: 10,
    centerSlides: true,
    // Responsive breakpoints
    navigation: {
        nextEl: '.swiper-button-next-about',
        prevEl: '.swiper-button-prev-about',
    },
    
    breakpoints: {
        // when window width is >= 320px
        300: {
            slidesPerView: 1.2,
            spaceBetween: 20
        },
        // when window width is >= 480px
        480: {
            slidesPerView: 1.5,
            spaceBetween: 30
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 40
        },
        // when window width is >= 640px
        1080: {
            slidesPerView: 2,
            spaceBetween: 40
        }
    }
})